module Geometry.Sphere
    ( volume
    , area
    ) where

import Prelude
    ( (*), (/), (^)
    , Float
    , Integer
    , pi
    )

volume :: Float -> Float
volume radius = (4.0 / 3.0) * pi * (radius ^ (3 :: Integer))

area :: Float -> Float
area radius = 4 * pi * (radius ^ (2 :: Integer))

-- From:
-- http://learnyouahaskell.com/modules
