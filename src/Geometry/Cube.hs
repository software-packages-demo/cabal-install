module Geometry.Cube
    ( volume
    , area
    ) where

import Geometry.Cuboid qualified as Cuboid
import Prelude
    ( Float
    )

volume :: Float -> Float
volume side = Cuboid.volume side side side

area :: Float -> Float
area side = Cuboid.area side side side

-- From:
-- http://learnyouahaskell.com/modules
