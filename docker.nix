{ pkgs ? import <nixpkgs> { system = "x86_64-linux";}
}:
pkgs.dockerTools.streamLayeredImage {
    name = "cabal-install-nix";
    tag = "latest";
    contents = [
        pkgs.cabal-install
        pkgs.ghc
    ];
    config = {};
}
