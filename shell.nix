{ pkgs ? import <nixpkgs> {}
}:
pkgs.mkShell {
    name="dev-environment";
    buildInputs = [
        pkgs.cabal-install
        pkgs.ghc
    ];
    shellHook = ''
        echo "Start developing..."
    '';
}
