#!/usr/bin/env cabal
{- cabal:
cabal-version: 3.4
build-depends: base
default-extensions:
    ImportQualifiedPost
    NoImplicitPrelude
    Safe
    Strict
ghc-options:
    -O0
    -Werror
    -Weverything
-}
{- project:
optimization: False
-}

module Main ( main ) where

import Prelude
    ( IO
    , putStrLn
    )

main :: IO ()
main = putStrLn "Hello, Haskell script!"

-- https://cabal.readthedocs.io/en/latest/cabal-commands.html#cabal-run
-- See also:
-- https://stackoverflow.com/questions/65539773/compiling-a-haskell-script-with-external-dependencies-without-cabal
