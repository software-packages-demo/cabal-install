import Geometry.Sphere qualified as Sphere
import Prelude
    ( ($), (++)
    , IO
    , putStrLn
    , show
    )

main :: IO ()
main = putStrLn $ "Sphere (radius 1) volume: " ++ show ( Sphere.volume 1 )
