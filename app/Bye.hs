module Main ( main ) where

import Prelude
    ( IO
    , putStrLn
    )

main :: IO ()
main = putStrLn "Bye, Haskell!"
